const axios = require('axios');
const config = require('config');

class PixabayApi {
  constructor() {}

  get apiKey() {
    return config.PIXABAY_API_KEY;
  }

  get baseUrl() {
    return 'https://pixabay.com/api/';
  }

  getImage(url) {
    return axios({ method: 'GET', responseType: 'stream', url });
  }

  search(query, page) {
    const options = {
      method: 'GET',
      params: {
        key: this.apiKey,
        q: query,
        image_type: 'photo',
        orientation: 'horizontal',
        page: page || 1,
        per_page: 200,
      },
      url: this.baseUrl
    };
    return axios(options).then((response) => response.data);
  }
}

module.exports = PixabayApi;
