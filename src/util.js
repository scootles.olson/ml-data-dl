const fs = require('fs');
const util = require('util');
const pExists = util.promisify(fs.exists);
const pMkdir = util.promisify(fs.mkdir);

module.exports.ensureDirExists = async (dir) => {
  const dirExists = await pExists(dir);
  if (!dirExists) await pMkdir(dir);
}
