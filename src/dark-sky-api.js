const axios = require('axios');
const config = require('config');

class DarkSkyApi {
  constructor() {}

  get apiKey() {
    return config.DARK_SKY_API_KEY;
  }

  get baseUrl() {
    return `https://api.darksky.net/forecast/${this.apiKey}`;
  }

  async forecast(lat, lon, timestamp) {
    const url = `${this.baseUrl}/${lat},${lon},${timestamp}`;
    return axios({ method: 'GET', responseType: 'stream', url });
  }
}

module.exports = DarkSkyApi;
