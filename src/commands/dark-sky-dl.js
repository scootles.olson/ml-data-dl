const async = require('async');
const { format, getUnixTime, parseISO, startOfDay, subDays } = require('date-fns');
const ProgressBar = require('progress');
const fs = require('fs');
const util = require('util');
const _ = require('lodash');

const DarkSkyApi = require('../dark-sky-api');
const { ensureDirExists } = require('../util');

class DarkSkyDl {
  constructor(lat, lon, options) {
    this.lat = lat;
    this.lon = lon;
    this.options = options;
    this.darkSkyApi = new DarkSkyApi();
    this._bar = null;
  }

  // get lat() {
  //   return this.options.lat;
  // }

  // get lon() {
  //   return this.options.lon;
  // }

  get timestamp() {
    const dt = this.options.timestamp && parseISO(this.options.timestamp) || new Date();
    return startOfDay(dt);
  }

  get num() {
    return this.options.num;
  }

  async run() {
    try {
      const timestamp = this.timestamp;
      const timestamps = _.map(_.times(this.num), (n) => { return subDays(timestamp, n); });
      this._bar = new ProgressBar('downloading [:bar] :current/:total :percent :etas', { total: timestamps.length });
      await async.eachLimit(timestamps, 2, this._processWebUrl.bind(null, this.lat, this.lon));
    }
    catch(err) {
      console.error(err);
    }
  }

  _processWebUrl = async (lat, lon, timestamp) => {
    const response = await this.darkSkyApi.forecast(lat, lon, getUnixTime(timestamp));
    await ensureDirExists('./data');
    const writer = fs.createWriteStream(`./data/dark-sky-${format(timestamp, 'yyyyMMdd')}.json`)
    response.data.pipe(writer);
    this._bar.tick();
  }
}

module.exports = DarkSkyDl;
