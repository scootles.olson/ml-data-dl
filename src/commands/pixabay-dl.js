const async = require('async');
const { format } = require('date-fns');
const ProgressBar = require('progress');
const fs = require('fs');
const util = require('util');
const pWriteFile = util.promisify(fs.writeFile);
const _ = require('lodash');

const PixabayApi = require('../pixabay-api');
const { ensureDirExists } = require('../util');

class PixabayDl {
  constructor(query, options) {
    this.query = query;
    this.options = options;
    this._pixabayApi = new PixabayApi();
    this._bar = null;
  }

  async run() {
    try {
      const data = await this._pixabayApi.search(this.query, this.options.page);
      // console.log('SEARCH', data);
      await ensureDirExists('./data');
      await ensureDirExists('./img');
      await pWriteFile(
        `./data/response-${format(new Date(), 'yyyyMMddHHmmss')}-${this.query}.json`,
        JSON.stringify(data, null, 2)
      );
      const webUrls = _.map(data.hits, 'webformatURL');
      this._bar = new ProgressBar('downloading [:bar] :current/:total :percent :etas', { total: webUrls.length });
      await async.eachLimit(webUrls, 2, this._processWebUrl);
    }
    catch(err) {
      console.error(_.get(err, 'response.data'));
    }
  }

  _processWebUrl = async (webUrl, d) => {
    const imgResponse = await this._pixabayApi.getImage(webUrl);
    const filename = _.last(imgResponse.headers['content-disposition'].match(/filename="([\w-\.]+)"/));
    const writer = fs.createWriteStream(`./img/${filename}`)
    imgResponse.data.pipe(writer);
    this._bar.tick();
  }
}

module.exports = PixabayDl;
