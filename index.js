#!/usr/bin/env node

const program = require('commander');
const _ = require('lodash');

const pkg = require('./package');
const DarkSkyDl = require('./src/commands/dark-sky-dl');
const PixabayDl = require('./src/commands/pixabay-dl');

program
  .version(pkg.version)
  .description('GAN functions for images.');

program
  .command('dark-sky <lat> <lon>')
  .description('Download weather data from Dark Sky.')
  .option('-n, --num <num>', 'Number of days to request data for', 1)
  .option('-t, --timestamp <timestamp>', 'The time to request weather data for')
  .action(async (lat, lon, options) => {
    const darkSkyDl = new DarkSkyDl(lat, lon, options);
    await darkSkyDl.run();
  });

program
  .command('pixabay-dl <query>')
  .description('Download images from pixabay.')
  .option('-p, --page <page>', 'The page to request', 1)
  .action(async (query, options) => {
    const pixabayDl = new PixabayDl(query, options);
    await pixabayDl.run();
  });

// Unspported Command
program
  .command('*')
  .action(() => { program.outputHelp(); });

if (!process.argv.slice(2).length) {
  program.outputHelp();
}

program.parse(process.argv);
